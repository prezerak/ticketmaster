﻿using System;
using System.Collections.Generic;

namespace TicketMaster.Data.Model
{
    public class Venue : Item
    {
        public String Name { get; set; }
        public int Availability { get; set; }
    }
}
