﻿using AutoMapper;
using TicketMaster.Api.Model;
using TicketMaster.Data.Model;

namespace TicketMaster
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {

            CreateMap<Act, ActJSON>().ReverseMap();
            //CreateMap<Venue, VenueJSON>().ReverseMap();
            
            CreateMap<Venue, VenueJSON>()
                .ForMember(dest => dest.Seats, opt => opt.MapFrom(src => src.Availability));
            CreateMap<VenueJSON, Venue>()
                .ForMember(dest => dest.Availability, opt => opt.MapFrom(src => src.Seats));
            
            CreateMap<Ticket, TicketJSONRequest>().ReverseMap();
            CreateMap<Ticket, TicketJSONReply>().ReverseMap();
            CreateMap<Show, ShowJSON>().ReverseMap();
        }
    }
}
